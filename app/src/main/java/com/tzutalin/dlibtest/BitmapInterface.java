package com.tzutalin.dlibtest;

import android.graphics.Bitmap;

public interface BitmapInterface {

    public void setBitmap(Bitmap bitmap);
}
